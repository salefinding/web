import {Routes, RouterModule} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {Pages} from './pages/pages.component';
import {NotfoundComponent} from './pages/notfound/notfound.component';

export const routes: Routes = [
  {path: '', component: Pages, pathMatch: 'full'},
  {path: '**', component: NotfoundComponent}
];
export const routing: ModuleWithProviders = RouterModule.forRoot(routes, { enableTracing: true , useHash: false});


