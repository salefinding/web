import {ProductDetail} from './product-detail';
export class Product {
  constructor(
    public id: number,
    public name: string,
    public image: string,
    public code: string,
    public max_sale_rate: string,
    public details: ProductDetail[]
  ) {}
}
