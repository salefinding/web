export class ProductDetail {
  constructor(
    public item_price: String,
    public old_price: String,
    public url: String,
    public country: String,
    public sale_rate: String
  ){}
}
