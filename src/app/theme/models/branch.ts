export class Branch {
  constructor(
    public id: number,
    public name: string,
    public categories: Object[],
    public keys: String[],
    public cols: number
  ) {}
}
