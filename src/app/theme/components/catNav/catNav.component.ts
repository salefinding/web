import {Component, OnInit, ViewChild} from '@angular/core';
import { NgIf } from '@angular/common';
import {SaleService} from '../../services/sale.service';
import {ProductComponent} from '../products';

@Component({
  selector: 'cat-nav',
  templateUrl: './catNav.component.html'
})

export class CatNavComponent implements OnInit {
  keys:  String[];
  cats: Object;
  current: string;
  parent: string;
  branchId: number;
  @ViewChild(ProductComponent)
  private productComponent: ProductComponent;
  constructor(private saleService: SaleService) {}
  loadCats() {
    this.saleService.getCats(this.branchId)
      .subscribe(
        cats => {
          this.keys = Object.keys(cats);
          this.cats = cats;
        },
        err => {
          console.log(err);
        }
      );
  }

  onSelect(current: string, parent: string) {
    this.current = current;
    this.parent = parent;
    this.productComponent.catChange(this.current, this.parent, this.branchId);
  }
  branchChange(id: number) {
    this.branchId = id;
    console.log('branch id ' + this.branchId);
    this.loadCats();
  }
  ngOnInit() {
    this.branchId = 1;
    this.loadCats();
  }
}
