import { Component, OnInit, ViewChild } from '@angular/core';
import {SaleService} from '../../services/sale.service';
import {Branch} from '../../models/branch';
import {CatNavComponent} from '../catNav';
import { FacebookService, InitParams } from 'ngx-facebook';
import {ProductComponent} from '../products';

@Component({
  selector: 'top-nav',
  templateUrl: './topNav.component.html',
  styleUrls: ['./topNav.css']
})
export class TopNavComponent implements OnInit {
  branchs: Branch[];
  branchId: number;
  current: string;
  parent: string;
  @ViewChild(CatNavComponent)
  private catNavComponent: CatNavComponent;
  @ViewChild(ProductComponent)
  private productComponent: ProductComponent;
  public name: number;
  constructor( private branchService: SaleService, private fb: FacebookService) {
    let initParams: InitParams = {
      appId: '1387307858182518',
      xfbml: true,
      version: 'v2.8'
    };

    //fb.init(initParams);
  }

  loadBranchs() {
    // Get all comments
    this.branchService.getBranchs()
      .subscribe(
        branchs => {
          this.branchs = branchs;
          console.log('branch length ' + this.branchs.length);
          this.branchs.map(value => {
              value.keys = Object.keys(value.categories);
              value.cols = Math.ceil(12 / value.keys.length);
          });
        }, // Bind to view
        err => {
          // Log errors if any
          console.log(err);
        });
  }

  ngOnInit() {
    // Load comments
    this.loadBranchs();
  }

  onClick(branchId: number) {
    this.branchId = branchId;
    this.catNavComponent.branchChange(this.branchId);
    this.name = branchId;
  }

  onSelect(current: string, parent: string, branchId: number) {
    this.current = current;
    this.parent = parent;
    this.branchId = branchId;
    this.productComponent.catChange(this.current, this.parent, this.branchId);
  }

  over() {
    console.log('over');
  }

}
