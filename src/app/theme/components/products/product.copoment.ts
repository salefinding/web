import {Component, OnInit} from '@angular/core';
import {SaleService} from '../../services/sale.service';
import {Product} from '../../models/product';

@Component({
  selector: 'product-list',
  templateUrl: './product.component.html'
})

export class ProductComponent implements OnInit {
  products: Product[];
  currentCat: string;
  parentCat: string;
  branchId: number
  constructor( private saleService: SaleService) {}

  loadProducts() {
    this.saleService.getProducts(this.currentCat, this.parentCat, this.branchId )
      .subscribe(
        products => this.products = products,
        err => {
          console.log(err);
        }
      );
  }

  ngOnInit() {
    this.loadProducts();
  }

  catChange(current, parent, branchId) {
    this.currentCat = current;
    this.parentCat = parent;
    this.branchId = branchId;
    console.log('cat id ' + this.branchId);
    this.loadProducts();
  }
}
