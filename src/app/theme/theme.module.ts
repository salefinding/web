import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SaleService } from './services/sale.service';
import { keyValueFilterPipe } from '../pipe/keyValueFilter.pipe';
import { FacebookModule } from 'ngx-facebook';

import {
  TopNavComponent,
  ProductComponent,
  CatNavComponent,
} from './components';

const THEME_COMPONENTS = [
  TopNavComponent,
  ProductComponent,
  CatNavComponent
];

@NgModule({
  declarations: [
    TopNavComponent,
    ProductComponent,
    CatNavComponent,
    keyValueFilterPipe
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    FacebookModule.forRoot(),
    ReactiveFormsModule,
  ],
  providers: [
    SaleService
  ],
  exports: [
    ...THEME_COMPONENTS
  ]
})

export class ThemeModule {

  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders> {
      ngModule: ThemeModule,
      providers: [
      ],
    };
  }
}
