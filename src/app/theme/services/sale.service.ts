// Imports
import { Injectable, ReflectiveInjector } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Branch } from '../models/branch';
import {Observable} from 'rxjs/Rx';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Product} from '../models/product';

@Injectable()
export class SaleService {
  private branchAPI = 'http://api.salefinding.com/branches';
  private catAPI = 'http://api.salefinding.com/categories/';
  private productAPI = 'http://api.salefinding.com/items';

  constructor (private http: Http) {}
  // Fetch all existing branchs
  getBranchs(): Observable<Branch[]> {
    // ...using get request
    return this.http.get(this.branchAPI)
    // ...and calling .json() on the response to return data
      .map((res: Response) => res.json())
      // ...errors if any
      .catch((error: any) => Observable.throw(error.json().error || console.log('Server error')));

  }

  getProducts(current, parent, branchId): Observable<Product[]> {
    let api;
    if ( current ) {
      api = this.productAPI + '/' + branchId + '/' + parent + '/' + current;
    } else {
      api = this.productAPI;
    }
    return this.http.get(api)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || console.log('Server error')));
  }

  getCats(id): Observable<Object> {
    return this.http.get(this.catAPI + id)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error || console.log('Server error')));
  }


}
