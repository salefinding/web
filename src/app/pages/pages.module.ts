import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Pages } from './pages.component';
import { ThemeModule } from '../theme/theme.module';

@NgModule({
  imports: [
    CommonModule,
    ThemeModule
  ],
  declarations: [Pages]
})
export class PagesModule { }
