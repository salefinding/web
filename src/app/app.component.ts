import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.css'],
  template: `<main>
    <router-outlet></router-outlet>
  </main>`
})
export class AppComponent {
  title = 'app works!';
}
