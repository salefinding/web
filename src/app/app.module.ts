import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import {CommonModule} from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FacebookModule } from 'ngx-facebook';

import {routing} from './app.routing';

import { AppComponent } from './app.component';
import { PagesModule } from './pages/pages.module';
import { NotfoundModule } from './pages/notfound/notfound.module';
import { ThemeModule } from './theme/theme.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule,
    HttpModule,
    JsonpModule,
    ThemeModule.forRoot(),
    PagesModule,
    CommonModule,
    NotfoundModule,
    routing,
    FacebookModule.forRoot()
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'en-US' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
